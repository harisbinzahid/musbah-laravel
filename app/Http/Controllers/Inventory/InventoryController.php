<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InventoryController extends Controller
{
    function inventoryMovement()
    {
      return View('pages.inventory.inventoryMovement');
    }

    function inventoryStatus()
    {
      return View('pages.inventory.inventoryStatus');
    }

    function reportsandAnalysis()
    {
      return View('pages.inventory.reportsandAnalysis');
    }
}
