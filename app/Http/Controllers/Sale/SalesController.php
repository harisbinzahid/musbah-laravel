<?php
namespace App\Http\Controllers\Sale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use \App\Libraries\Helper;
use Redirect;

class SalesController extends Controller
{
    public function salesQuotationInquiry(){
    	return View('pages.sales.salesQuotationInquiry');
    }
    
    public function getSalesQuotationInquiry(){
    	$data = DB::select("
	    				SELECT 
						sorder.order_no,
						sorder.reference,
						debtor.name,
						branch.br_name,
						sorder.customer_ref,
						sorder.ord_date,
						sorder.delivery_date,
						sorder.deliver_to,
						Sum(line.unit_price*line.quantity*line.sizeTotal)+freight_cost AS OrderValue,
						sorder.type,
						debtor.curr_code,
						Sum(line.qty_sent) AS TotDelivered,
						Sum(line.quantity) AS TotQuantity,
						line.size as size 
						FROM 
						0_sales_orders as sorder,
						0_sales_order_details as line,
						0_debtors_master as debtor,
						0_cust_branch as branch 
						WHERE sorder.order_no = line.order_no 
							AND sorder.trans_type = line.trans_type 
							AND sorder.trans_type = '32' 
							AND sorder.debtor_no = debtor.debtor_no 
							AND sorder.branch_code = branch.branch_code 
							AND debtor.debtor_no = branch.debtor_no 
							AND sorder.ord_date >= '2010-09-16' 
							AND sorder.ord_date <= '2019-10-17' GROUP BY sorder.order_no,
							sorder.debtor_no,
							sorder.branch_code,
							sorder.customer_ref,
							sorder.ord_date,
							sorder.deliver_to
         ");

    	return View('pages.sales.partials.salesQuo', ['data' => $data]);
    }
    public function customerTransiction(){
    	return View('pages.sales.customerTransictions');
    }



    public function customerAllocationInquiry()
    {

    	return View('pages.sales.customerAllocationInquiry');
    }

    public function reportsandAnalysis()
    {

    	return View('pages.sales.reportsandAnalysis');
    }
}


