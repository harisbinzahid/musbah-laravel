<?php

namespace App\Http\Controllers\SystemGL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use \App\Libraries\Helper;


class SystemController extends Controller
{
  public function journalInquiry()
  {
    return View('pages.system-GL.journalInquiry');
  }


  public function glInquiry()
  {
    return View('pages.system-GL.glInquiry');
  }

  public function bankAccountInquiry()
  {
    return View('pages.system-GL.bankAccountInquiry');
  }

  public function taxInquiry()
  {
    return View('pages.system-GL.taxInquiry');
  }

  public function trialBalance()
  {
    return View('pages.system-GL.trialBalance');
  }

  public function balanceDrilldown()
  {
    return View('pages.system-GL.balanceDrilldown');
  }

}
