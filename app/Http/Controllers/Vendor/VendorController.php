<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorController extends Controller
{
    public function requestQuotation()
    {
    	return View('pages.vendor.requestQuotation');
    }

    public function searchPurchaseOrders()
    {
    	return View('pages.vendor.searchPurchaseOrders');
    }


    public function supplierAllocationInquiry()
    {
    	return View('pages.vendor.supplierAllocationInquiry');
    }

    public function supplierTransInquiry()
    {
        return View('pages.vendor.supplierTransInquiry');
    }

    public function reportsandAnalysis()
    {
        return View('pages.vendor.reportsandAnalysis');
    }
}

