<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;




class LoginController extends Controller
{
    public function Login()
    {
    	return view ('login');
    }

    public function Register()
    {
    	return view ('register');
    }

    public function Forgot()
    {
    	return view ('forgot');
    }

   
}