<?php

namespace App\Libraries;
use DB;

class Helper
{


static function getSalesPrice(){
  $data = DB::table('0_prices')
            ->where('sales_type_id','=',$_POST['sales_type'])
            ->where('stock_id','=',$_POST['stock_id'])
            ->get();
  if(count($data) > 0)
    return $data[0]->price;
  else
    return 0;

}


static function supplier_list($selected_id = null, $name = null){
  if($name == null)
    $name = 'supplier_id';
//  $selected = $selected_id ? 'selected' : '';
 // $sql = "SELECT supplier_id, supp_ref, curr_code, inactive FROM ".TB_PREF."suppliers ";

  $cust = DB::table('0_suppliers')->get();
  $supp = '<select required="required" name="'.$name.'" class="form-control" id="'.$name.'">
  <option value="">Select Supplier </option>';
        foreach ($cust as $value) {
        $supp .= '<option value="'.$value->supplier_id.'">'.$value->supp_ref.'</option>';
        }
      $supp .= '</select>';
        echo $supp;

}


static function get_company_pref($pref){
  $pref = DB::table('0_sys_prefs')->where('name', $pref)->get();
  return $pref[0]->value;
}

static function get_salesman($id){
  $salesman = DB::table('0_salesman')->where('salesman_code', $id)->get();
  return $salesman[0]->salesman_name;
}

static function date2sql($date){
  $date = str_replace('/', '-', $date);
  return date('Y-m-d', strtotime($date));
}

static function sql2date($date){
 // $date = str_replace('/', '-', $date);
  return date('d/m/Y', strtotime($date));
}

static function getLocationName($loc){
  $locname = DB::table('0_locations')->where('loc_code', $loc)->get();
  return $locname[0]->location_name;
}

static function getLocationShortName($loc){
  $locname = DB::table('0_locations')->where('location_name', $loc)->get();
  return $locname[0]->loc_code;
}

public static function from_location($row){
  if($row->type == 16)
  {
  $loc = DB::table('0_stock_moves')
  ->where('trans_no','=',$row->trans_no)
  ->where('type','=',$row->type)
  ->where('qty','<','0')
  ->get();
  return self::getLocationName($loc[0]->loc_code);
  }
  else
  return '';
}

public static function from_location_SR($row){
  if($row->type == 101)
  {
  $loc = DB::table('0_stock_request')
  ->where('trans_no','=',$row->trans_no)
  ->where('type','=',$row->type)
  ->where('qty','<','0')
  ->get();
  return self::getLocationName($loc[0]->loc_code);
  }
  else
  return '';
}

public static function to_location($row){
  if($row->type == 16)
  {
  $loc = DB::table('0_stock_moves')
  ->where('trans_no','=',$row->trans_no)
  ->where('type','=',$row->type)
  ->where('qty','>','0')
  ->get();
  return self::getLocationName($loc[0]->loc_code);
  }
  else if($row->type == 17)
  {
  $loc = DB::table('0_stock_moves')
  ->where('trans_no','=',$row->trans_no)
  ->where('type','=',$row->type)
  ->get();
  return self::getLocationName($loc[0]->loc_code);
  }
  else
  return '';
}

public static function to_location_SR($row){
  if($row->type == 101)
  {
  $loc = DB::table('0_stock_request')
  ->where('trans_no','=',$row->trans_no)
  ->where('type','=',$row->type)
  ->where('qty','>','0')
  ->get();
  return self::getLocationName($loc[0]->loc_code);
  }
  else
  return '';
}



public static function getStockInformation($stock_id){
  $stock = DB::table('0_stock_master')->where('stock_id', $stock_id)->get();
  return $stock[0];
}


static function item_rate_new($stock_id = null){
  $price = DB::table('0_purch_data')->select('price')->where('stock_id', $stock_id)->get();
  return $price[0];
}

static function stock_item_rate($stock_id = null){
  $price = DB::table('0_purch_data')->where('stock_id', $stock_id)->get();
  return $price;
}

static function item_rate(){
  $stock_id = $_POST['stock_id'];
  $price = DB::table('0_purch_data')->select('price')->where('stock_id', $stock_id)->get();
  return response()->json($price[0]->price);
}


static function get_customer_trans($trans_no,$type){
  $trans =  DB::table('0_debtor_trans')
  ->join('0_debtors_master', '0_debtors_master.debtor_no','=','0_debtor_trans.debtor_no')
  ->join('0_sales_types', '0_sales_types.id','=','0_debtor_trans.tpe')
  ->leftjoin('0_comments', function($join1){
  $join1->on('0_debtor_trans.type','=','0_comments.type')
        ->on('0_debtor_trans.trans_no','=','0_comments.id');
  })
  ->leftjoin('0_shippers', function($join2){
  $join2->on('0_shippers.shipper_id','=','0_debtor_trans.ship_via');
  })
  ->leftjoin('0_cust_branch', function($join3){
  $join3->on('0_debtor_trans.branch_code','=','0_cust_branch.branch_code');
  })
  ->join('0_tax_groups', '0_cust_branch.tax_group_id','=','0_tax_groups.id')

  ->select(
  '0_debtor_trans.*',
  DB::raw('(0_debtor_trans.ov_amount + 0_debtor_trans.ov_gst + 0_debtor_trans.ov_freight + 0_debtor_trans.ov_freight_tax +
  0_debtor_trans.ov_discount)
  AS Total'),
  '0_debtors_master.name AS DebtorName',
  '0_debtors_master.address',
  '0_debtors_master.curr_code',
  '0_debtors_master.tax_id',
  DB::raw('0_debtor_trans.prep_amount>0 as prepaid'),
  '0_comments.memo_',
  '0_shippers.shipper_name',
  '0_sales_types.sales_type',
  '0_sales_types.tax_included',
  '0_cust_branch.*',
  '0_debtors_master.discount',
  '0_tax_groups.name AS tax_group_name',
  '0_tax_groups.id AS tax_group_id ')
  ->where([
  ['0_debtor_trans.trans_no','=',$trans_no],
  ['0_debtor_trans.type','=',$type],

  ])
  ->get();

  return $trans ;

}


static function get_customer_trans_details($trans_no, $type){
  $line_item = DB::table('0_debtor_trans_details')
  ->join('0_stock_master', '0_stock_master.stock_id', '=', '0_debtor_trans_details.stock_id')
  ->select('0_debtor_trans_details.*',DB::raw('0_debtor_trans_details.unit_price+0_debtor_trans_details.unit_tax AS FullUnitPrice'),
  '0_debtor_trans_details.description As StockDescription','0_stock_master.units','0_stock_master.mb_flag')
  ->where('0_debtor_trans_details.debtor_trans_no','=',$trans_no)
  ->where('0_debtor_trans_details.debtor_trans_type','=',$type)
  ->get();

  return $line_item;
}


static function customer_list($selected_id = null, $name = null){
  if($name == null)
    $name = 'customer_id';
  $selected = $selected_id ? 'selected' : '';
  $cust = DB::table('0_debtors_master')->where('inactive','=',0)->get();
  $ret = '<select required="required" name="'.$name.'" class="form-control" id="'.$name.'">
  <option value="">Select Customer</option>';
  foreach ($cust as $value) {
  $ret .= '<option '.$selected.' value="'.$value->debtor_no.'">'.$value->debtor_ref.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}
//Haris ->:
static function journal_inquiry_list($selected_id = null, $name = null){
  if($name == null)
    $name = 'customer_id';
  $selected = $selected_id ? 'selected' : '';
  $cust = array
    (
    '1'=> _("Journal Entry"),
    '2'=> _("Bank Payment"),
    '3' => _("Bank Deposit"),
    '4' => _("Funds Transfer"),
    '5'=> _("Sales Invoice"),
    '6'=> _("Customer Credit Note"),
    '7' => _("Customer Payment"),
    '8' => _("Delivery Note"),
    '9' => _("Inventory Adjustment"),
    '10' => _("Supplier Invoice"),
    '11'=> _("Supplier Credit Note"),
    '12'=> _("Supplier Payment"),
    '13' => _("Purcahse Order Delivery"),
    '14' => _("Work Order"),
    '15'=> _("Work Order Issue"),
    '16'=> _("Work Order Production"),
    '17' => _("Cost Update")

    );
  $ret = '<select required="required" name="'.$name.'" class="form-control" id="'.$name.'">
  <option value="">All Types</option>';
  foreach ($cust as $id=>$value) {
  $ret .= '<option '.$selected.' value="'.$id.'">'.$value.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}

static function account_inquiry($selected_id = null, $name = null){
  if($name == null)
    $name = 'customer_id';
  $selected = $selected_id ? 'selected' : '';
  $cust = array
    (
    '1'=> _("Current Account"),
    '2'=> _("Default Card Cash Account"),
    '3' => _("Default Petty Cash Account"),
    '4' => _("Funds Transfer"),
    '5'=> _("EIB"),
    '6'=> _("Petty Cash Account")

    );
  $ret = '<select required="required" name="'.$name.'" class="form-control" id="'.$name.'">
  <option value="">Select Account</option>';
  foreach ($cust as $id=>$value) {
  $ret .= '<option '.$selected.' value="'.$id.'">'.$value.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}

static function cust_allocations_list_cells($selected_id = null, $name = null){
  if($name == null)
    $name = 'customer_id';
  $selected = $selected_id ? 'selected' : '';
  $cust = array
    (
    '1'=> _("Sales Invoices"),
    '2'=> _("Overdue Invoices"),
    '3' => _("Payments"),
    '4' => _("Credit Notes")
    );
  $ret = '<select required="required" name="'.$name.'" class="form-control" id="'.$name.'">
  <option value="">All Types</option>';
  foreach ($cust as $id=>$value) {
  $ret .= '<option '.$selected.' value="'.$id.'">'.$value.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}
//Haris <-:

// static function cust_allocations_list_cells()
// {

// $allocs = array
//     (
//     $all_items=>_("All Types"),
//     '1'=> _("Sales Invoices"),
//     '2'=> _("Overdue Invoices"),
//     '3' => _("Payments"),
//     '4' => _("Credit Notes")
//     )
// //     $ret = '<select>';
//     foreach ($allocs as $id=>$value) {
//   $ret .= '<option id='.$id.' value="'.$value.'">'.$value.'</option>';
//   }
//   $ret .= '</select>';
//   echo $ret;
// }



static function bank_account_list($name = null){
  if($name == null)
    $name = 'bank_account';
  $acc = DB::table('0_bank_accounts')->get();
  $ret = '<select required="required" name="'.$name.'" class="input-sm form-control" id="'.$name.'">
  <option value="">Select Account</option>';
  foreach ($acc as $value) {
  $ret .= '<option value="'.$value->id.'">'.$value->bank_account_name.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}

static function get_bank_account_name($id){
  $acc = DB::table('0_bank_accounts')->where('id','=',$id)->get();
  return $acc[0]->bank_account_name;
}


static function get_bank_account_code($id){
  $acc = DB::table('0_bank_accounts')->where('id','=',$id)->get();
  return $acc[0]->account_code;
}


static function stock_master_list($selected_id = null){
  $selected = $selected_id ? 'selected' : '';
  $item = DB::table('0_stock_master')->get();
  $ret = '<select name="stock_id" class=" form-control" id="stock_id">
  <option value="">Select Item</option>';
  foreach ($item as $value) {
  $ret .= '<option '.$selected.' value="'.$value->stock_id.'">'.$value->stock_id.' - '.$value->description.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}


static function driver_expense_account_list($name = null){

  if($name == null)
    $name = 'account_code';
  $data = DB::select("SELECT chart.account_code, chart.account_name, type.name, chart.inactive, type.id
			FROM 0_chart_master chart, 0_chart_types type
			WHERE chart.account_type=type.id AND type.id = 12
ORDER BY `chart`.`account_code` ASC");

  $ret = '<select name="'.$name.'" class="input-sm form-control" id="'.$name.'">
    <optgroup label="'.$data[0]->name.'">';
  foreach ($data as $value) {
  $ret .= '<option value="'.$value->account_code.'">'.$value->account_code.'      '.$value->account_name.'</option>';
  }

  $ret .= '<optgroup/></select>';
  echo $ret;

}


static function sale_payment_list($name)
{
  $data = DB::table('0_payment_terms')->select('terms_indicator', 'terms', 'inactive')->get();
  $ret = '<select name="'.$name.'" class="input-sm form-control" id="'.$name.'">
  <option value="">Select Payment</option>';
  foreach ($data as $value) {
  $ret .= '<option value="'.$value->terms_indicator.'">'.$value->terms.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}

static function sales_types_list($name){
  $data = DB::table('0_sales_types')->get();
  $ret = '<select name="'.$name.'" class="input-sm form-control" id="'.$name.'">
  <option value="">Select Sales Type</option>';
  foreach ($data as $value) {
  $ret .= '<option value="'.$value->id.'">'.$value->sales_type.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}

static function shippers_list($name){
  $data = DB::table('0_shippers')->get();
  $ret = '<select required="required"  name="'.$name.'" class="input-sm form-control" id="'.$name.'">';
  foreach ($data as $value) {
  $ret .= '<option value="'.$value->shipper_id.'">'.$value->shipper_name.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}


static function locations_list($name=null){
  $data = DB::table('0_locations')->get();
  $ret = '<select required="required"  name="'.$name.'" class="input-sm form-control" id="'.$name.'">';
  foreach ($data as $value) {
  $ret .= '<option value="'.$value->loc_code.'">'.$value->location_name.'</option>';
  }

  $ret .= '</select>';
  echo $ret;
}


static function get_next_reference($name, $type=null){
   $ref = DB::table('0_refs')->where('type','=',$type)->max('reference');
   $reference = $ref + 1;
   $ret = '<input readonly type="text" class="input-sm form-control" data-trigger="change" data-required="true" data-type="number" value='.$reference.'
    placeholder="reference" name="'.$name.'" id="'.$name.'">';
   echo $ret;

}

static function get_items($stock_id){  return DB::table('0_stock_master')
  ->join('0_item_tax_types','0_item_tax_types.id','=','0_stock_master.tax_type_id')
  ->select(DB::raw('0_stock_master.*'),'0_item_tax_types.name as tax_type_name')
  ->where('stock_id','=',$stock_id)->get();

}

static function get_branch_accounts($branch_code){
  return DB::table('0_cust_branch')->select('receivables_account','sales_account', 'sales_discount_account', 'payment_discount_account')->where('branch_code','=',1)->get();
}

static function get_stock_gl_code($stock_id){
  return DB::table('0_stock_master')->select('mb_flag', 'inventory_account', 'cogs_account', 'adjustment_account', 'sales_account', 'wip_account', 'dimension_id', 'dimension2_id')
  ->where('stock_id','=',$stock_id)->get();
}

function get_item_edit_info($stock_id=null){
  if(@$_POST['stock_id'])
      $stock_id = @$_POST['stock_id'];

  return  DB::table('0_stock_master as item')
  ->join('0_item_units as unit', 'unit.abbr','=','item.units')
  ->select('item.markup','item.description','item.purchase_cost','item.material_cost', 'item.units', 'unit.decimals')
  ->where('stock_id','=',$stock_id)
  ->get();
}

function get_item_markup($stock_id=null){
  if($_POST['stock_id'])
  $stock_id = $_POST['stock_id'];

  $data = DB::table('0_stock_master as item')
  ->join('0_item_units as unit', 'unit.abbr','=','item.units')
  ->select('item.markup','item.description','item.purchase_cost','item.material_cost', 'item.units', 'unit.decimals')
  ->where('stock_id','=',$stock_id)
  ->get();
  return $data[0]->markup;
}

function get_qoh_on_date($stock_id = null, $location=null, $date_=null){
  if(@$_POST['stock_id'])
     $stock_id = $_POST['stock_id'];
  if(@$_POST['location'])
     $location = $_POST['location'];

  if ($date_ == null)
  $date_ = date('Y-m-d');

  if ($location != null){
  $matchThese = [
   ['0_stock_moves.stock_id','=',$stock_id],
   ['0_stock_moves.tran_date','<=',$date_],
   ['0_stock_moves.loc_code','=',$location],
  ];
  }else{
  $matchThese = [
   ['0_stock_moves.stock_id','=',$stock_id],
   ['0_stock_moves.tran_date','<=',$date_],
   ];
  }
  $data =  DB::table('0_stock_moves')
  ->leftjoin('0_voided', function($join){
     $join->on('0_stock_moves.type','=','0_voided.type')
          ->on('0_stock_moves.trans_no','=','0_voided.id');
  })
  ->whereNull('0_voided.id')
  ->where($matchThese)->sum('qty');

  return $data;


}

static function getSalesType($id){
  return DB::table('0_sales_types')->select('sales_type')->where('id','=',$id)->get();
}


static function getPaymentTerms($id){
  return DB::table('0_payment_terms')->select('terms')->where('terms_indicator','=',$id)->get();
}

static function bank_accounts_list($name){
  $data = DB::table('0_bank_accounts')->select('id', 'bank_account_name', 'bank_curr_code', 'inactive')->get();
  $ret = '<select name="'.$name.'" class="input-sm form-control" id="'.$name.'">
  <option value="">Select Account</option>';
  foreach ($data as $value) {
  $ret .= '<option value="'.$value->id.'">'.$value->bank_account_name.'</option>';
  }
  $ret .= '</select>';
  echo $ret;

}

static function get_bank_account($id){
    return DB::table('0_bank_accounts')->where('id','=',$id)->get();
}

static function document_list($name, $selected_id = null){
  $selected = $selected_id ? 'selected' : '';
  $doc = DB::table('0_document')->where('inactive','=',0)->get();
  $ret = '<select required="required"  name="'.$name.'" class="input-sm form-control" id="'.$name.'">
  <option value="">Select Document</option>';
  foreach ($doc as $value) {
  $ret .= '<option '.$selected.' value="'.$value->id.'">'.$value->document.'</option>';
  }
  $ret .= '</select>';
  echo $ret;
}

}
