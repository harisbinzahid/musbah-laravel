<!-- /footer -->

	<!-- Core JS files -->
	<script type="text/javascript" src="/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->
	<!-- Theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="/js/plugins/pickers/pickadate/legacy.js"></script>

	<script type="text/javascript" src="/js/core/app.js"></script>
	<script type="text/javascript" src="/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="/js/plugins/ui/ripple.min.js"></script>
	<!-- Theme JS files -->






	<script>

    $(".daterange-single").daterangepicker({
        locale: {
        }
    });
	</script>

	<div class="footer text-muted text-center">
			&copy; 2017. <a href="#">Musbah ERP</a> by <a href="http://wayz.ae" target="_blank">WayZ Consultant</a>
	</div>

<!-- /footer -->


</html>
