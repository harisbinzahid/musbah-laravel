<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="/dashboard"><img src="/images/wayz.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown language-switch">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<!-- <img src="/images/demo/users/face11.jpg" alt=""> -->
							<span>Admin</span>
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu">
						<li><a href="/login"> <i class="icon-switch2"> </i> Logout </a></li>

					</ul>
				</li>

		</div>
	</div>
	<!-- /main navbar -->


<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<!-- <a href="/dashboard" class="media-left"><img src=" /images/demo/users/face11.jpg" class="img-circle img-sm" alt=""></a -->
								<div class="media-body">
									<span class="media-heading text-semibold">Musbah - ERP</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Dubai, LLC
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
								<li><a href="/dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li>
									<a href="#"><i class="icon-users4"></i> <span>Customers</span></a>
									<ul>
										<li>
											<a href="#">Inquiries & Reports</a>
											<ul>
												<li><a href="/sales/salesQuotationInquiry">Sales Quotation Inquiry</a></li>
												<li><a href="/sales/customerTransictions">Customer Transiction Inquiry</a></li>
												<li><a href="/sales/customerAllocationInquiry">Customer Allocation Inquiry</a></li>
												<li><a href="/sales/reportsandAnalysis">Customer and Sales Reports</a></li>
												<!-- <li><a href="3_col_dual.html">Customer Allocation Inquiry</a></li>
												<li><a href="3_col_double.html">Customer & Sales Reports</a></li> -->
											</ul>

										</li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-people"></i> <span>Vendors</span></a>
									<ul>
										<li>
											<a href="#">Inquiries & Reports</a>
											<ul>
												<li><a href="/vendor/requestQuotation">Request for Quotation</a></li>
												<li><a href="/vendor/searchPurchaseOrders">Purchase Orders Inquiry</a></li>
												<li><a href="/vendor/supplierTransInquiry">Supplier Transiction Inquiry</a></li>
												<li><a href="/vendor/supplierAllocationInquiry">Supplier Allocation Inquiry</a></li>
												<li><a href="/vendor/reportsandAnalysis">Supplier & Purchasing Reports</a></li>
											</ul>

										</li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Inventory</span></a>
									<ul>
										<li>
											<a href="#">Inquiries & Reports</a>
											<ul>
												<li><a href="/inventory/inventoryMovement">Inventory Item Movement</a></li>
												<li><a href="/inventory/inventoryStatus">Inventory Item Status</a></li>
												<li><a href="/inventory/reportsandAnalysis">Reports & Analysis</a></li>
												<!-- <li><a href="/inventory/inventoryStatus">Customer & Sales Reports</a></li> -->
											</ul>

										</li>
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-coin-dollar"></i> <span>System & GL</span></a>
									<ul>
										<li>
											<a href="#">Inquiries & Reports</a>
											<ul>
												<li><a href="/system-GL/journalInquiry">Journal Inquiry</a></li>
												<li><a href="/system-GL/glInquiry">GL Inquiry</a></li>
												<li><a href="/system-GL/bankAccountInquiry">Bank Account Inquiry</a></li>
												<li><a href="/system-GL/taxInquiry">Tax Inquiry</a></li>
												<li><a href="/system-GL/trialBalance">Trial Balance</a></li>
												<li><a href="/system-GL/balanceDrilldown">Balance Sheet Drilldown</a></li>
											</ul>

										</li>
									</ul>
								</li>

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
<!-- Main content -->
			<div class="content-wrapper">
