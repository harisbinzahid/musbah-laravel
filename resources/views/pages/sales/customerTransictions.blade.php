@include('inc.header')
@include('inc.nav')
@inject('helper', \App\Libraries\Helper)

<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">Customer Transictions</span></h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href=""><i class="icon-home2 position-left"></i> Customer</a></li>
         <li><a href="">Inquiries & Reports</a></li>
         <li class="active">Customer Transictions</li>
		</ul>

	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
<div class="panel panel-flat">
	<div class="panel-body">
		<label class="align-items-end">For Invoice Search through i-INVOICE-ID and for Customer Payment search through c-CHEQUE-NO</label>
		<div class="row">
		<div class="form-group col-md-4">
			<label class="control-label col-lg-4">Doc Search:</label>
				<input type="text" class="form-control" placeholder="Search">
		</div>

		<div class="form-group col-md-4">
			<label> From: </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="icon-calendar22"></i></span>
				<input type="text" class="form-control daterange-single" value="{{ date('m/d/Y') }} ">
			</div>
		</div>
		<div class="form-group col-md-4">
			<label> To: </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="icon-calendar22"></i></span>
				<input type="text" class="form-control daterange-single" value="{{ date('m/d/Y') }} ">
			</div>
		</div>
	</div>

<div class="row">
				<div class="form-group col-md-4">
			<label> Item:  </label>
			<div class="input-group">
				{{ $helper::stock_master_list() }} 
			</div>
				</div>

			<div class="form-group col-md-4">
				<label> Select a Customer:  </label>
				<div class="input-group">
					{{ $helper::customer_list() }} 
				</div>
			</div>
		</div>

</div>
</div>

@include('inc.footer')

</div>
<!-- Content area -->




