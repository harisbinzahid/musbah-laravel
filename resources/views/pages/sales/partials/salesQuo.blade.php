@inject('helper', \App\Libraries\Helper)
<div class="table-responsive">
               <table class="table">
                  <thead>
                     <tr>
                        <th>Quote #</th>
                        <th>Ref</th>
                        <th>Customer</th>
                        <th>Branch</th>
                        <th>Cust Order Ref</th>
                        <th>Quote Date</th>
                        <th>Valid until</th>
                        <th>Delivery To</th>
                        <th>Quote Total</th>
                        <th>Currency</th>
                        <th class="text-center"></th>
                     </tr>
                  </thead>
                  <tbody>
                  	@foreach($data as $value)
                     <tr>
                        <td><a>{{ $value->order_no }}<a/></td>
                        <td>{{ $value->reference }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->br_name }}</td>
                        <td>{{ $value->customer_ref }}</td>
                        <td>{{ $helper::sql2date($value->ord_date) }}</td>
                        <td>{{ $helper::sql2date($value->delivery_date) }}</td>
                        <td>{{ $helper::sql2date($value->deliver_to) }}</td>
                        <td>{{ number_format($value->OrderValue,2) }}</td>
                        <td>{{ $value->curr_code }}</td>
                        <td><a href="#"><i class="icon-file-pdf"></i></a></td>
                     </tr>
                	@endforeach
                  </tbody>
               </table>