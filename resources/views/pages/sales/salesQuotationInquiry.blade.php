@include('inc.header')
@include('inc.nav')
@inject('helper', \App\Libraries\Helper)
<!-- /page header -->
<div class="page-header page-header-default">
   <div class="page-header-content">
      <div class="page-title">
         <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"> Sales Quotation Inquiry</span></h4>
      </div>
   </div>
   <div class="breadcrumb-line">
      <ul class="breadcrumb">
         <li><a href=""><i class="icon-home2 position-left"></i> Customer</a></li>
         <li><a href="">Inquiries & Reports</a></li>
         <li class="active">Sales Quotation Inquiry</li>
      </ul>
   </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

   <div class="panel panel-flat">
      <div class="panel-body">


		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
		            <div class="form-group">
		               <label class="control-label" style="font-weight: bold"> Invoice #:</label>
		               <input type="text" class="form-control" id="invoice" placeholder="Search through Invoice #">
		            </div>					
				</div>
				<div class="col-md-4">
				<div class="form-group">
					<label class="control-label" style="font-weight: bold">From: </label>
					<div class="input-group">
						<span class="input-group-addon"><i class="icon-calendar22"></i></span>
						<input type="text" class="form-control daterange-single" id="fromdate" value="{{ date('d/m/Y') }}">
					</div>
				</div>
				</div>
				<div class="col-md-4">
		            <div class="form-group">
					<label class="control-label" style="font-weight: bold">To: </label>
					<div class="input-group">
						<span class="input-group-addon"><i class="icon-calendar22"></i></span>
						<input type="text" class="form-control daterange-single" id="todate"   value="{{ date('d/m/Y') }}">
					</div>
		            </div>							
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-4">
		            <div class="form-group">
		               <label class="control-label" style="font-weight: bold"> Ref #:</label>
		               <input type="text" class="form-control" id="ref"  placeholder="Search through Invoice #">
		            </div>					
				</div>
				<div class="col-md-4">
				<div class="form-group">
					<label class="control-label" style="font-weight: bold">Items: </label>
		                  {{ $helper::stock_master_list() }} 
				</div>
				</div>
				<div class="col-md-4">
		            <div class="form-group">
					<label class="control-label" style="font-weight: bold">Customer: </label>
		                  {{ $helper::customer_list() }} 

		            </div>							

				</div>

			</div>


		</div>
         
         <div class="salesQuo"></div>

         </div>
         <!-- /column controlled child rows -->
      </div>
      @include('inc.footer')
   </div>

<!-- Content area -->




<script>
	
$(document).ready(function() {
$.ajax({
    url:'/sales/getSalesQuotationInquiry',
    success: function(response){
     $('.salesQuo').html(response);

    }
  });
  
});

// $('#customer_id').on('change', function() {

// var customer_id = $('#customer_id').val();
// alert(customer_id);

// $.ajax({
//     url:'/sales/getSalesQuotationInquiry/'+customer_id,
//     success: function(response){
//      $('.salesQuo').html(response);

//     }
//   });
  
// });
</script>   