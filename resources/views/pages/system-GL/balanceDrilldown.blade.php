@include('inc.header')
@include('inc.nav')
@inject('helper', \App\Libraries\Helper)
<!-- /page header -->
<div class="page-header page-header-default">
   <div class="page-header-content">
      <div class="page-title">
         <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Balance Sheet Drilldown</span></h4>
      </div>
   </div>
   <div class="breadcrumb-line">
      <ul class="breadcrumb">
         <li><a href=""><i class="icon-home2 position-left"></i> System and GL</a></li>
         <li><a href="">Inquiries & Reports</a></li>
         <li class="active">Balance Sheet Drilldown</li>
      </ul>
   </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

   <div class="panel panel-flat">
      <div class="panel-body">


		<div class="row">
			<div class="col-md-12">

				<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" style="font-weight: bold">As at: </label>
					<div class="input-group">
						<span class="input-group-addon"><i class="icon-calendar22"></i></span>
						<input type="text" class="form-control daterange-single" id="fromdate" value="{{ date('d/m/Y') }}">
					</div>
				</div>
				</div>

			</div>
            </div>

		</div>

         <div class="salesQuo"></div>

         </div>
         <!-- /column controlled child rows -->

               @include('inc.footer')
      </div>


<!-- Content area -->



<script>

$(document).ready(function() {
$.ajax({
    url:'/sales/getSalesQuotationInquiry',
    success: function(response){
     $('.salesQuo').html(response);

    }
  });

});




// $('#customer_id').on('change', function() {

// var customer_id = $('#customer_id').val();
// alert(customer_id);

// $.ajax({
//     url:'/sales/getSalesQuotationInquiry/'+customer_id,
//     success: function(response){
//      $('.salesQuo').html(response);

//     }
//   });

// });
</script>
