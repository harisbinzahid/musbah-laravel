@include('inc.header')
@include('inc.nav')
@inject('helper', \App\Libraries\Helper)
<!-- /page header -->
<div class="page-header page-header-default">
   <div class="page-header-content">
      <div class="page-title">
         <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"> Reports and Analysis</span></h4>
      </div>
   </div>
   <div class="breadcrumb-line">
      <ul class="breadcrumb">
         <li><a href=""><i class="icon-home2 position-left"></i> Vendors</a></li>
         <li><a href="">Inquiries & Reports</a></li>
         <li class="active">Reports and Analysis</li>
      </ul>
   </div>
</div>
<div class="content">
   <div class="panel panel-flat">
      <div class="panel-body">
        

         <div class="col-md-4">
         <div class="row col-sm-9">
            <label>Report Classes:</label>
           <li> <a href=" ">Customer</a></li>
            <li><a href="">Supplier</a></li>
            <li> <a href=" ">Inventory</a></li>
            <li><a href=" ">Manufacturing</a></li>
            <li><a href=" ">Dimensions</a></li>
            <li><a href=" ">Banking</a></li>
            <li><a href=" ">General Ledger</a></li>
         </div>
         </div>

         <div class="col-md-4">
            <label>Reports For Class:  Customer</label>
           <li> <a href="">Customer Balances</a></li>
           <li> <a href=" ">Customer account Statement</a></li>
           <li> <a href="">Customer PDC Balances</a>  </li>
           <li> <a href="">Daily Closing Report</a>   </li>
           <li> <a href="">Aged Customer Analysis</a></li>
           <li> <a href="">Customer Detail Listing</a></li>
           <li> <a href="">Sales Summary Report</a></li>
           <li> <a href="">Price Listing</a></li>
           <li> <a href="">Order Status Listing</a></li>
           <li> <a href="">Salesman Listing</a></li>
           <li> <a href="">Print Invoices</a></li>
           <li> <a href="">Print Invoices New</a></li>
           <li> <a href="">Print Credit Notes</a></li>
           <li> <a href="">Print Deliveries</a></li>
           <li> <a href="">Print Statements</a></li>
           <li> <a href="">Print Sales Orders</a></li>
           <li> <a href="">Print Sales Quotations</a></li>
           <li> <a href="">Print Receipts</a></li>
         </div>


         <div class="col-md-6">
         </div>
</div>

</div>



@include('inc.footer')
</div>

            
            
            
            
            
            

