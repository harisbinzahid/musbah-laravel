@include('inc.header')

@inject('helper', \App\Libraries\Helper)



<body class="login-container">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href=""><img src="images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->
<!-- @if(Session::has('message'))
<h4 style="
    width: 75%;
    margin-left: 14%;
    font-size: 15px;
    font-weight: lighter;
" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</h4>
@endif -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Advanced login -->
					<form action="getregister" method="POST">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
								<h5 class="content-group">Create account <small class="display-block">All fields are required</small></h5>
							</div>

							<div class="content-divider text-muted form-group"><span>Your credentials</span></div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" name="email" class="form-control" placeholder="Email" required="required">
								<div class="form-control-feedback">
									<i class="icon-user-check text-muted"></i>
								</div>
								<!-- <span class="help-block text-danger"><i class="icon-cancel-circle2 position-left"></i> This username is already taken</span> -->
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" name="password" class="form-control" placeholder="Create password" required="required">
								<div class="form-control-feedback">
									<i class="icon-user-lock text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" name="confrm_pass" class="form-control" placeholder="Confirm password" required="required">
								<div class="form-control-feedback">
									<i class="icon-user-lock text-muted"></i>
								</div>
							</div>

							<!-- <div class="content-divider text-muted form-group"><span>Your privacy</span></div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Your email">
								<div class="form-control-feedback">
									<i class="icon-mention text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" placeholder="Repeat email">
								<div class="form-control-feedback">
									<i class="icon-mention text-muted"></i>
								</div>
							</div> -->

							<!-- <div class="content-divider text-muted form-group"><span>Additions</span></div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" class="styled" checked="checked">
										Send me <a href="#">test account settings</a>
									</label>
								</div>

								<div class="checkbox">
									<label>
										<input type="checkbox" class="styled" checked="checked">
										Subscribe to monthly newsletter
									</label>
								</div>

								<div class="checkbox">
									<label>
										<input type="checkbox" class="styled">
										Accept <a href="#">terms of service</a>
									</label>
								</div>
							</div> -->

							<button type="submit" class="btn bg-teal btn-block btn-lg">Register <i class="icon-circle-right2 position-right"></i></button>
						</div>
						{{csrf_field()}}
					</form>
					<!-- /advanced login -->


					<!-- Footer -->
					<div class="footer text-muted text-center">
						&copy; 2017. <a href="#"> Musbah ERP </a> by <a href="http://wayz.ae" target="_blank">WayZ Consultant</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<script>
		
$(document).ready(function() {

$.ajax({
    url:'/Register/Store',
    success: function(response){
     $('register').html(response);

    }
  });

});

	</script>