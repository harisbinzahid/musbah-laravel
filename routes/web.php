<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('dashboard', 'DashboardController@Dashboard');

Route::get('login', 'LoginController@Login');
Route::post('getregister', 'RegisterController@Store');

Route::get('register', 'LoginController@Register');

Route::get('forgot', 'LoginController@Forgot');

// SALES ROUTES

Route::get('sales/salesQuotationInquiry', 'Sale\SalesController@salesQuotationInquiry');
Route::get('sales/customerTransictions', 'Sale\SalesController@customerTransiction');
Route::get('sales/getSalesQuotationInquiry', 'Sale\SalesController@getSalesQuotationInquiry');
Route::get('sales/customerAllocationInquiry', 'Sale\SalesController@customerAllocationInquiry');
Route::get('sales/reportsandAnalysis', 'Sale\SalesController@reportsandAnalysis');


// VENDORS ROUTES
Route::get('vendor/requestQuotation', 'Vendor\VendorController@requestQuotation');
Route::get('vendor/searchPurchaseOrders', 'Vendor\VendorController@searchPurchaseOrders');
Route::get('vendor/supplierAllocationInquiry', 'Vendor\VendorController@supplierAllocationInquiry');
Route::get('vendor/supplierTransInquiry', 'Vendor\VendorController@supplierTransInquiry');
Route::get('vendor/reportsandAnalysis', 'Vendor\VendorController@reportsandAnalysis');


// INVENTORY ROUTES
Route::get('inventory/inventoryMovement', 'Inventory\InventoryController@inventoryMovement');
Route::get('inventory/inventoryStatus', 'Inventory\InventoryController@inventoryStatus');
Route::get('inventory/reportsandAnalysis', 'Inventory\InventoryController@reportsandAnalysis');

// systemGL ROUTES
Route::get('system-GL/journalInquiry', 'SystemGL\SystemController@journalInquiry');
Route::get('system-GL/glInquiry', 'SystemGL\SystemController@glInquiry');
Route::get('system-GL/bankAccountInquiry', 'SystemGL\SystemController@bankAccountInquiry');
Route::get('system-GL/taxInquiry', 'SystemGL\SystemController@taxInquiry');
Route::get('system-GL/trialBalance', 'SystemGL\SystemController@trialBalance');
Route::get('system-GL/balanceDrilldown', 'SystemGL\SystemController@balanceDrilldown');







// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
